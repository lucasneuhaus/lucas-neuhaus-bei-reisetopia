## Lucas Neuhaus bei reisetopia
  
  
Dies ist ein einfaches Plugin für WordPress.
Es registriert ein neuen Shortcode: `[ln_text]`.
Der Shortcode wird dort wo er plaziert wird durch einen Text ersetzt der im Menu (LN bei reisetopia) festgelegt werden kann.

### Anmerkungen
Die index.php datei hat keine weiter funktion und existiert nur aus sicherheitsgründen,  
um den direkten zugriff auf die Datein des Plugins zu verhindern.