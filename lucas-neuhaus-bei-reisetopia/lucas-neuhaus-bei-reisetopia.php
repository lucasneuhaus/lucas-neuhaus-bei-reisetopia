<?php
/**
 * @package lucas-neuhaus-bei-reisetopia
 * @version 1.0
 */
/*
Plugin Name: Lucas Neuhaus bei reisetopia
Description: This is a simple Plugin to add a new Shortcode that will display a Text that is defined in the wp-admin panel.
Author: Lucas Neuhaus
Version: 1.0
*/

// check if this file is actually executed by wordpress
defined( 'ABSPATH') or die('Can´t acces this file.');

// create the menu page add the text option and register the shortcode
ln_add_actions_and_shortcode();

// initialize the settings
function ln_settings_init() {
    // register a new setting for the ln page
    register_setting('ln', 'ln_option');

    // register a section on the ln page
    add_settings_section(
        'ln_section_dev',
        __('Variable Text Shortcode.', 'ln'),
        'ln_section_dev_cb',
        'ln'
    );

    // register a field in the ln_section_dev section
    add_settings_field(
        'ln_text_field',
        'Text:',
        'ln_text_field_cb',
        'ln',
        'ln_section_dev',
        [
            'label for' => 'ln_text_field',
            'ln_custom_data' => 'custom',
        ]
    );
}

// options page setup
function ln_options_page() {
    add_menu_page(
        'Lucas Neuhaus bei reisetopia!',
        'LN bei reisetopia',
        'manage_options',
        'ln',
        'ln_options_page_html',
        'dashicons-edit'
    );
}

// html for the options page
function ln_options_page_html() {
    // check user capabilities
    if (! current_user_can( 'manage_options')) {
        return;
    }

    // check if the user have submitted the settings
    if( isset( $_GET['settings-updated'] ) ) {
        add_settings_error('ln_messages', 'ln_message', __('Settings Saved', 'ln'), 'updated');
    }

    // show err messages
    settings_errors( 'ln_messages');

    ?>
    <div class="wrap">
        <h1><?php echo esc_html( get_admin_page_title()); ?></h1>
        <form action='options.php' method='post'>
            <?php
            settings_fields('ln');
            do_settings_sections('ln');
            submit_button('Save Shortcode!');
            ?>
        </form>
    </div>
    <?php
}

// callback function for the option description
function ln_section_dev_cb( $args) {
    ?>
    <p id="<?php echo esc_attr( $args['id'] ); ?>">
        <?php esc_html_e('Hier kann ein Text eingegeben werden, der mit dem Shortcode [ln_text] plaziert werden kann.',
            'ln'); ?>
    </p>
    <?php
}

// callback function for the <textarea> tag
function ln_text_field_cb() {
    // getting the data from the option
    $option_val = esc_attr( get_option('ln_option'));
    ?>
    <textarea   type="text"
                rows="4"
                cols="50"
                name="ln_option"
                id="ln_option"
                placeholder="Gebe hier den Text ein..."
    ><?php echo $option_val; ?></textarea>
    <?php
}

// function to be executed by the shortcode
function ln_option_shortcode() {
    return esc_attr(get_option('ln_option'));
}

// function used to add actions to hooks and register the shortcode
function ln_add_actions_and_shortcode() {
    // add the setting_init function to the admin_init hook
    add_action( 'admin_init', 'ln_settings_init');

    // register the option_page
    add_action('admin_menu', 'ln_options_page');

    // add the shortcode [ln_text]
    add_shortcode('ln_text', 'ln_option_shortcode');
}